# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import json
import csv
import tkinter as tk
import traceback
from tkinter.filedialog import askopenfilename, asksaveasfilename


class TurnItOut():

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("TurnItOut")
        self.infile = "None selected"
        self.outfile = "None selected"
        ltitle = tk.Label(text="TurnitOut - Turnitin to Excel conversion")
        ltitle.pack()
        self.bchoosein = tk.Button(text="Select RBC file", command=self.readfile)
        self.bchoosein.pack()
        self.lchoosein = tk.Label(text=self.infile)
        self.lchoosein.pack()
        self.bchooseout = tk.Button(text="Select output CSV file", command=self.writefile)
        self.bchooseout.pack()
        self.lchooseout = tk.Label(text=self.outfile)
        self.lchooseout.pack()
        self.bprocess= tk.Button(text="Process rubric", command=self.process_rubric)
        self.bprocess.pack()
        self.bprocess.configure(state=tk.DISABLED)
        self.lprocess = tk.Label(text="Select input and output files")
        self.lprocess.pack()

    def readfile(self):
        self.infile = askopenfilename(title = 'Select RBC file',
                                      filetypes = (('RBC files', '*.rbc'),('All files', '*.*')))
        self.lchoosein.configure(text=self.infile)
        self.check_files()

    def writefile(self):
        self.outfile = asksaveasfilename(title = 'Select CSV file',
                                      filetypes = (('CSV files', '*.csv'),('All files', '*.*')))
        self.lchooseout.configure(text=self.outfile)
        self.check_files()

    def check_files(self):
        if self.infile != "None selected" and self.outfile != "None selected":
            self.bprocess.configure(state=tk.ACTIVE)
            self.lprocess.configure(text="")

    def process_rubric(self):
        try:
            self.read_rubric()
            self.write_rubric()

        except Exception as e:
            self.message = "An Error Occurred"
            traceback.print_exc()
        self.lprocess.configure(text=self.message)
    def read_rubric(self):
        fh = open(self.infile)
        self.rubric = json.load(fh)
        self.rubric_criterion_list = []
        self.rubric_criterion_elements = {}
        self.rubric_scale_list = []
        self.rubric_scale_elements = {}
        self.rubric_criterion_scale_list = []
        self.rubric_criterion_scale_elements = {}
        self.rubric_name = self.rubric['Rubric'][0]['name']
        self.rubric_criterion_list = self.rubric['Rubric'][0]['criterion']
        self.rubric_scale_list = self.rubric['Rubric'][0]['scale_values']
        for e in self.rubric['RubricScale']:
            self.rubric_scale_elements[e['id']]=e
        for e in self.rubric['RubricCriterion']:
            self.rubric_criterion_elements[e['id']] = e
            self.rubric_criterion_scale_elements[e['id']]={}
        for e in self.rubric['RubricCriterionScale']:
            self.rubric_criterion_scale_elements[e['criterion']][e['scale_value']]=e
        self.message="JSON read"


    def write_rubric(self):
        #build up the rows
        rows = [[self.rubric_name]+ ['']*(len(self.rubric_scale_list)+2),['','',''],['Title','Description', 'Percentage']]
        for e in self.rubric_scale_list:
            rows[1].append(self.rubric_scale_elements[e]['name'])
            rows[2].append(self.rubric_scale_elements[e]['value'])
        for e in self.rubric_criterion_list:
            rows.append([self.rubric_criterion_elements[e]['name'],self.rubric_criterion_elements[e]['description'],self.rubric_criterion_elements[e]['value']])
            for s in self.rubric_scale_list:
                rows[-1].append(self.rubric_criterion_scale_elements[e].get(s, {'description':''})['description'])
        self.message='Rows complete'
        with open(self.outfile, 'w', newline='') as csvfile:
            csv.writer(csvfile).writerows(rows)
        self.message = 'CSV file written'


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    win = TurnItOut()
    win.window.mainloop()


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
